const config = require('./config/development');
var request = require('request');
var express = require('express');
var app = express()
var port = process.env.PORT || config.PORT;
var server = require('http').createServer(app)
var FBBotFramework = require('fb-bot-framework');
var async = require('async');
var _ = require('underscore');

// Initialize
var bot = new FBBotFramework({
  page_token: config.page_token,
  verify_token: config.verify_token
});

var namhoc = '', hocki = '', mssv = '';

// Setup Express middleware for /webhook
app.use('/webhook', bot.middleware());

// Setup listener for incoming messages 
bot.on('message', function(userId, message){
    // bot.sendTextMessage(userId, "Echo Message:" + message); 
    switch (message.toLowerCase()) {
        case 'tkb':
            // Chọn năm học
            var text = "Vui lòng chọn năm học:";
            var buttons = [
                {
                    "content_type":"text",
                    "title": "2012-2013",
                    "payload": "Y|2012-2013"
                },
                {
                    "content_type":"text",
                    "title": "2013-2014",
                    "payload": "Y|2013-2014"
                },
                {
                    "content_type":"text",
                    "title": "2014-2015",
                    "payload": "Y|2014-2015"
                },
                {
                    "content_type":"text",
                    "title": "2015-2016",
                    "payload": "Y|2015-2016"
                },
                {
                    "content_type":"text",
                    "title": "2016-2017",
                    "payload": "Y|2016-2017"
                },
                {
                    "content_type":"text",
                    "title": "2017-2018",
                    "payload": "Y|2017-2018"
                }
            ];
            bot.sendQuickReplies(userId, text, buttons);
            break;
        // case 'photo':
        //     bot.sendImageMessage(userId, 'http://childhub.fpo.vn/img/screenshot_1.png');
        //     break;
        case 'hello':
        case 'hi':
            bot.getUserProfile(userId, function (err, profile) {
                // console.log(profile);
                bot.sendTextMessage(userId, 'Hello ' + (profile.gender == 'male' ? 'anh ' : 'chị ') + profile.first_name + ' ' + profile.last_name);
                bot.sendImageMessage(userId, profile.profile_pic);
            });
            break;
        // case 'button':
        //     var menuButtons = [
        //         {
        //             "type": "postback",
        //             "title": "Trợ giúp",
        //             "payload": "DEVELOPER_DEFINED_PAYLOAD_FOR_HELP"
        //         },
        //         {
        //             "type": "web_url",
        //             "title": "Tìm hiểu FPO",
        //             "url": "http://www.fpo.vn/"
        //         }
        //     ];
        //     bot.setPersistentMenu(menuButtons);
        //     break;
        default:
            if (message[0].toLowerCase() == 'k' || message[0].toLowerCase() == '4') {
                mssv = message
                bot.sendTextMessage(userId, "Năm học: " + namhoc + "; Học kì: " + hocki + "; MSSV: " + mssv)
                request.post({url:'http://202.78.227.73:2000/getTKB', form: {namhoc: namhoc, hocki: hocki, mssv: mssv}}, function(err,httpResponse,body){ 
                    var data = JSON.parse(body);
                    var info = 'Họ tên: ' + data.hoten + '\r\n'+ 
                                'Hệ: ' + data.he + '\r\n' +
                                'Loại hình đào tạo: ' + data.loaihinh + '\r\n' + 
                                'Lớp: ' + data.lop + '\r\n' + 
                                '#️⃣Tổng chỉ: ' + data.tongTinChi + '\r\n'+ 
                                '💰Tổng tiền: ' + data.tongTien;

                    bot.sendTextMessage(userId, info, function(callback) {
                        _.each(data.tkb, (item, idx, list) => {
                            var mess = '📚📚📚 *' + item[2] + '* 📚📚📚\r\n' + 
                                        '💳Mã HP: ' + item[1] + '\r\n' +
                                        '🔢Số chỉ: ' + item[3] + '\r\n' +
                                        '💰Số tiền: ' + item[4] + '\r\n' +
                                        '📅Lịch học: ' + item[5] + '\r\n' +
                                        '👨‍🏫Giảng viên: ' + item[6] + '\r\n' +
                                        '🕛Ngày BĐ: ' + item[7] + '\r\n' +
                                        '🕛Ngày KT: ' + item[8] + '\r\n' +
                                        '⏰Ngày ĐK: ' + item[9] + '\r\n';
                            bot.sendTextMessage(userId, mess);
                        })
                    });
                });
            }
            break;
    }    
});

bot.on('postback', function(userId, payload) {
    if (payload == 'DEVELOPER_DEFINED_PAYLOAD_FOR_HELP') {
        bot.sendTextMessage(userId, 'Xin chào, tôi là FPO Bot - hệ thống trả lời tự động với khả năng xử lý ngôn ngữ tự nhiên. Rất vui được trò chuyện với bạn.');
        bot.sendTextMessage(userId, 'Hiện tại tôi chỉ có tính năng xem Thời khoá biểu cho sinh viên trường Đại học Sư phạm Tp.HCM');
    }
});
 
// Setup listener for quick reply messages 
bot.on('quickreply', function(userId, payload){
    switch (payload.split('|')[0]) {
        case 'Y':
            namhoc = payload.split('|')[1]
            var text = "Vui lòng chọn học kì:";
            var buttons = [
                {
                    "content_type":"text",
                    "title": "Học kì 1",
                    "payload": "S|1"
                },
                {
                    "content_type":"text",
                    "title": "Học kì 2",
                    "payload": "S|2"
                },
                {
                    "content_type":"text",
                    "title": "Học kì hè",
                    "payload": "S|3"
                },
            ];
            bot.sendQuickReplies(userId, text, buttons);
            break
        case 'S':
            hocki = 'HK0' + payload.split('|')[1]
            bot.sendTextMessage(userId, "Nhập MSSV")
            break
    }
});


 
bot.setGreetingText('Hãy nhấn Get started hay Bắt đầu để trò chuyện với chúng tôi nhé', function(err, result) {})
 
app.get('/', function (req, res){
  res.send('hello world');
});

//Make Express listening
server.listen(port, function() {
    console.log("Listening on port %s...", server.address().port)
});